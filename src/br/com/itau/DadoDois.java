package br.com.itau;

import java.util.Random;

public class DadoDois {

    public static void main(String[] args) {
        Random random = new Random();
        int numero1 = random.nextInt(5) + 1;
        int numero2 = random.nextInt(5) + 1;
        int numero3 = random.nextInt(5) + 1;
        int somaNumeros = numero1+numero2+numero3;

        System.out.println(numero1 + "," + numero2 + "," + numero3 + "," + somaNumeros);
    }

}
